FROM node:latest
LABEL maintainer="Aneyze Santos<aneyzes@gmail.com>"
RUN  mkdir /todomvc
COPY ./.idea/newcalculator.html /todomvc
WORKDIR /todomvc

RUN mv newcalculator.html index.html
RUN npm update


EXPOSE 80

ENTRYPOINT npx serve -l 80